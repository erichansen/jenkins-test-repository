import json
import os

path = 'c:/Users/Eric/Documents/Siuvo/JenkinsTestRepository/Schema_Forms/'
english_names = set()
chinese_names = set()
missing_chinese = set()
enform_cnform = dict()
missing_features = set()
questions_dont_match = dict()
bad_whitespace = set()

outfile = open('c:/Users/Eric/Documents/Siuvo/JenkinsTestRepository/output.txt', 'w', encoding='utf8')

for file in os.listdir(path):
    with open(path+file, 'r', encoding='utf8') as f:
        form_data = json.load(f)
        for k, v in form_data.items():
            if k == "shufuFormId":
                if "_cn" in v:
                    chinese_names.add(v)
                else:
                    english_names.add(v)
            else:
                pass

for name in english_names:
    name_length = len(list(name))
    permutations = set()
    for i in range(0, name_length + 1):
        permutations.add(name[:i] + "_cn" + name[i:])
    if permutations.intersection(chinese_names) == set():
        missing_chinese.add(name)
    else:
        for x in permutations.intersection(chinese_names):
            enform_cnform[name] = x
            
outfile.write("The following forms are missing Chinese versions:\n") 
outfile.write("___________________________________________________\n")       
for x in sorted(missing_chinese):
    outfile.write(x+'\n')
outfile.write("___________________________________________________\n\n\n")

for file in os.listdir(path):
    with open(path+file, 'r', encoding='utf8') as f:
        form_data = json.load(f)
        if "|" not in str(form_data):
            missing_features.add(file)

outfile.write("The following forms are missing SNOMED codes:\n")
outfile.write("___________________________________________________\n")
for x in sorted(missing_features):
    x = x.rsplit('.')
    outfile.write('.'.join(x[:-1])+'\n')
outfile.write("___________________________________________________\n\n\n")

def english_question_search(x):
    for key, value in x.items():
        try:
            if 'enum' in value:
                english_questions.append(key)
            elif isinstance(value, dict):
                english_question_search(value)
            else:
                pass
        except: TypeError

def chinese_question_search(x):
    for key, value in x.items():
        try:
            if 'enum' in value:
                chinese_questions.append(key)
            elif isinstance(value, dict):
                chinese_question_search(value)
            else:
                pass
        except: TypeError

for k, v in enform_cnform.items():
    with open(path+k+".json", 'r', encoding='utf8') as f:
        englishForm = json.load(f)
    with open(path+v+".json", 'r', encoding='utf8') as g:
        chineseForm = json.load(g)
    english_questions = []
    english_question_search(englishForm)
    chinese_questions = []
    chinese_question_search(chineseForm)
    if english_questions != chinese_questions:
        questions_dont_match[k] = v
    else:
        pass            

outfile.write("The following pairs of forms have questions that don't match up:\n") 
outfile.write("___________________________________________________\n")   
for k, v in questions_dont_match.items():
    outfile.write(k+"    -    "+v+'\n')
outfile.write("___________________________________________________\n\n\n")

for file in os.listdir(path):
    with open(path+file, 'r', encoding='utf8') as f:
        form_data = json.load(f)
        if " ' " in str(form_data) or " '," in str(form_data) or " '\n" in str(form_data):
            bad_whitespace.add(file)
            

outfile.write("The following forms have leading or trailing whitespace in their keys or values:\n")
outfile.write("___________________________________________________\n")
for x in sorted(bad_whitespace):
    x = x.rsplit('.')
    outfile.write('.'.join(x[:-1])+'\n')
outfile.write("___________________________________________________\n\n\n")

ORDER_PROBLEM = set()
NO_BIG_ORDER = set()

for file in os.listdir(path):
    with open(path+file, 'r', encoding='utf8') as F:
        FORM_DATA = json.load(F)
    try:
        BIG_ORDER = FORM_DATA['order']
        for x in BIG_ORDER:
            SMALL_ORDER = FORM_DATA['properties'][x]['order']
            QUESTIONS = []
            for k in FORM_DATA['properties'][x]['properties'].keys():
                QUESTIONS.append(k)
            if SMALL_ORDER == QUESTIONS:
                pass
            else:
                ORDER_PROBLEM.add(file)
    except KeyError:
        NO_BIG_ORDER.add(file)

outfile.write("The following forms lack an overall order:\n") 
outfile.write("___________________________________________________\n")       
for x in sorted(NO_BIG_ORDER):
    x = x.rsplit('.')
    outfile.write('.'.join(x[:-1])+'\n')
outfile.write("___________________________________________________\n\n\n")

outfile.write("The following forms have a problem with their orders:\n") 
outfile.write("___________________________________________________\n")       
for x in sorted(ORDER_PROBLEM):
    x = x.rsplit('.')
    outfile.write('.'.join(x[:-1])+'\n')
outfile.write("___________________________________________________\n\n\n")

outfile.close()  

print("Results written to output.txt.")      
   